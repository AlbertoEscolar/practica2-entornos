package Interfaz;
/** 
 * @author Alberto Escolar
 * @since 10-1-18 
 */ 
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JToolBar;
import javax.swing.JScrollBar;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JRadioButton;
import javax.swing.JList;
import javax.swing.ButtonGroup;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.JCheckBox;
import javax.swing.JTree;
import javax.swing.JTextPane;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;

public class ventana extends JFrame {

	private JPanel contentPane;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField txtAltura;
	private JTextField txtAnchura;
	private JTextField txtTamaopx;
	private JTable table;
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ventana frame = new ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ventana() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(ventana.class.getResource("/imagenes/TecladoIcono.jpg")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 784, 505);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnAbrir = new JMenu("Abrir");
		menuBar.add(mnAbrir);
		
		JMenu mnNuevo = new JMenu("Nuevo");
		mnAbrir.add(mnNuevo);
		
		JMenuItem mntmModeloConPlantilla = new JMenuItem("Modelo con plantilla");
		mnNuevo.add(mntmModeloConPlantilla);
		
		JMenuItem mntmTecladoVacio = new JMenuItem("Teclado vacio");
		mnNuevo.add(mntmTecladoVacio);
		
		JMenu mnImportar = new JMenu("Importar");
		mnAbrir.add(mnImportar);
		
		JMenuItem mntmSeleccionarArchivo = new JMenuItem("Seleccionar archivo");
		mnImportar.add(mntmSeleccionarArchivo);
		
		JMenu mnGuardar = new JMenu("Guardar");
		mnAbrir.add(mnGuardar);
		
		JMenuItem mntmGuardarComo = new JMenuItem("Guardar como..");
		mnGuardar.add(mntmGuardarComo);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnGuardar.add(mntmGuardar);
		
		JMenu mnEditar = new JMenu("Editar");
		menuBar.add(mnEditar);
		
		JMenuItem mntmFormato = new JMenuItem("Formato");
		mnEditar.add(mntmFormato);
		
		JMenuItem mntmEstilo = new JMenuItem("Estilo");
		mnEditar.add(mntmEstilo);
		
		JMenuItem mntmFuente = new JMenuItem("Fuente");
		mnEditar.add(mntmFuente);
		
		JMenuItem mntmTamao = new JMenuItem("Tama\u00F1o");
		mnEditar.add(mntmTamao);
		
		JMenu mnApariencia = new JMenu("Apariencia");
		menuBar.add(mnApariencia);
		
		JMenu mnVentanas = new JMenu("Ventanas");
		mnApariencia.add(mnVentanas);
		
		JMenuItem mntmEditor = new JMenuItem("Editor");
		mnVentanas.add(mntmEditor);
		
		JMenuItem mntmTemas = new JMenuItem("Temas");
		mnVentanas.add(mntmTemas);
		
		JMenuItem mntmBarraDeHerramientas = new JMenuItem("Barra de herramientas");
		mnVentanas.add(mntmBarraDeHerramientas);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(260, 302, 483, 130);
		lblNewLabel.setIcon(new ImageIcon(ventana.class.getResource("/imagenes/Avid-Media-Composer.jpg")));
		contentPane.add(lblNewLabel);
		
		JToolBar toolBar_1 = new JToolBar();
		toolBar_1.setBackground(Color.RED);
		toolBar_1.setBounds(0, 0, 84, 40);
		contentPane.add(toolBar_1);
		
		JList list = new JList();
		toolBar_1.add(list);
		
		JRadioButton rdbtnLpiz = new JRadioButton("L\u00E1piz");
		buttonGroup.add(rdbtnLpiz);
		toolBar_1.add(rdbtnLpiz);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(745, 0, 21, 432);
		contentPane.add(scrollBar);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Plataforma:", "Ipad", "Tablet(Android)", "Smartphone"}));
		comboBox.setToolTipText("");
		comboBox.setBounds(91, 329, 96, 22);
		contentPane.add(comboBox);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBackground(Color.RED);
		toolBar.setBounds(0, 40, 84, 40);
		contentPane.add(toolBar);
		
		JRadioButton rdbtnPincel = new JRadioButton("Pincel");
		buttonGroup.add(rdbtnPincel);
		toolBar.add(rdbtnPincel);
		
		JToolBar toolBar_2 = new JToolBar();
		toolBar_2.setBackground(Color.RED);
		toolBar_2.setBounds(0, 80, 84, 40);
		contentPane.add(toolBar_2);
		
		JRadioButton rdbtnBrocha = new JRadioButton("Brocha");
		buttonGroup.add(rdbtnBrocha);
		toolBar_2.add(rdbtnBrocha);
		
		JToolBar toolBar_3 = new JToolBar();
		toolBar_3.setBackground(Color.RED);
		toolBar_3.setBounds(0, 119, 84, 40);
		contentPane.add(toolBar_3);
		
		JRadioButton rdbtnTexto = new JRadioButton("Texto");
		buttonGroup.add(rdbtnTexto);
		toolBar_3.add(rdbtnTexto);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(542, 267, 30, 22);
		contentPane.add(spinner);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setBounds(678, 267, 30, 22);
		contentPane.add(spinner_1);
		
		txtAltura = new JTextField();
		txtAltura.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtAltura.setText("Altura");
		txtAltura.setBounds(477, 267, 62, 22);
		contentPane.add(txtAltura);
		txtAltura.setColumns(10);
		
		txtAnchura = new JTextField();
		txtAnchura.setText("Anchura");
		txtAnchura.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtAnchura.setColumns(10);
		txtAnchura.setBounds(600, 267, 75, 22);
		contentPane.add(txtAnchura);
		
		txtTamaopx = new JTextField();
		txtTamaopx.setText("Tama\u00F1o (px)");
		txtTamaopx.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtTamaopx.setColumns(10);
		txtTamaopx.setBounds(343, 267, 104, 22);
		contentPane.add(txtTamaopx);
		
		JSlider slider = new JSlider();
		slider.setMajorTickSpacing(10);
		slider.setPaintLabels(true);
		slider.setPaintTicks(true);
		slider.setBounds(30, 244, 200, 45);
		contentPane.add(slider);
		
		JTextArea txtrOpacidad = new JTextArea();
		txtrOpacidad.setText("OPACIDAD");
		txtrOpacidad.setBounds(91, 209, 75, 22);
		contentPane.add(txtrOpacidad);
		
		table = new JTable();
		table.setBorder(new LineBorder(new Color(0, 0, 0), 3));
		table.setBounds(343, 236, 365, -195);
		contentPane.add(table);
		
		JCheckBox chckbxCrearTema = new JCheckBox("Crear tema");
		buttonGroup_1.add(chckbxCrearTema);
		chckbxCrearTema.setBounds(131, 40, 113, 25);
		contentPane.add(chckbxCrearTema);
		
		JCheckBox chckbxCrearTexto = new JCheckBox("Crear texto");
		buttonGroup_1.add(chckbxCrearTexto);
		chckbxCrearTexto.setBounds(131, 117, 113, 25);
		contentPane.add(chckbxCrearTexto);
		
		JTextPane txtpnElegirFuente = new JTextPane();
		txtpnElegirFuente.setFont(new Font("Tahoma", Font.PLAIN, 17));
		txtpnElegirFuente.setText("Elegir fuente:");
		txtpnElegirFuente.setBounds(477, 7, 113, 33);
		contentPane.add(txtpnElegirFuente);
		
		JTree tree = new JTree();
		tree.setModel(new DefaultTreeModel(
			new DefaultMutableTreeNode("Tipograf\u00EDas") {
				{
					DefaultMutableTreeNode node_1;
					node_1 = new DefaultMutableTreeNode("colores");
						node_1.add(new DefaultMutableTreeNode("azul"));
						node_1.add(new DefaultMutableTreeNode("violeta"));
						node_1.add(new DefaultMutableTreeNode("rojo"));
						node_1.add(new DefaultMutableTreeNode("amarillo"));
					add(node_1);
					node_1 = new DefaultMutableTreeNode("estilo");
						node_1.add(new DefaultMutableTreeNode("Tahoma"));
						node_1.add(new DefaultMutableTreeNode("Times New Roman"));
						node_1.add(new DefaultMutableTreeNode("Arial"));
						node_1.add(new DefaultMutableTreeNode("Romans"));
					add(node_1);
					node_1 = new DefaultMutableTreeNode("Tama\u00F1o");
						node_1.add(new DefaultMutableTreeNode("11"));
						node_1.add(new DefaultMutableTreeNode("12"));
						node_1.add(new DefaultMutableTreeNode("13"));
						node_1.add(new DefaultMutableTreeNode("14"));
						node_1.add(new DefaultMutableTreeNode("15"));
						node_1.add(new DefaultMutableTreeNode("+"));
					add(node_1);
				}
			}
		));
		tree.setBounds(618, 13, 113, 250);
		contentPane.add(tree);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(ventana.class.getResource("/imagenes/swiftkey.jpg")));
		lblNewLabel_1.setBounds(260, 40, 346, 214);
		contentPane.add(lblNewLabel_1);
	}
}
