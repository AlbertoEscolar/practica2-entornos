package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		char caracter=3;
		String cadenaLeida, cadenaFinal;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();
		
		cadenaLeida.indexOf(0, caracter); //En este caso el error viene dado por la variable posicionCaracter ya que al estar cogiendo con el IndexOf un car�cter que est� establecido en 27 no encuentra la posici�n de una cadena, aparte se debe poner dentro del par�ntesis del IndexOf un valor inicial para coger los caracteres de la cadena
		//Como no encuentra ning�n valor para la cadena se le asigna el -1
		
		cadenaFinal = cadenaLeida.substring(0, caracter);
		//Si establecemos el valor de car�cter en 3 y en la cadenaLeida iniciamos el IndexOf desde 0 y en la cadenaFinal cambiamos la finalizaci�n de la selecci�n de caracteres en el valor del car�cter (3), de esta forma la cadena ser� recortada hasta el numero de valores que se indica en la variable car�cter en este caso las tres primeras letras
		System.out.println(cadenaFinal);
		
		
		lector.close();
	}

}
