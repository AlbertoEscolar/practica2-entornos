package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qué realiza este programa
		 */
		/*Este programa de lo que se encarga es de los restos que se producen al dividir el numero introducido 
		/por los numeros anteriores al mismo, de esta forma si en m�s de dos ocasiones el numero se puede dividir 
		*con un resultado de resto 0, querr� decir que el numero no es primo, ya que los numeros primos
		* solo se pueden dividir por ellos mismos y por 1.
		*/
		
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
