package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		double resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		for(int i = numeroLeido; i >= 1 ; i--){ //Este programa tiene dos principales problemas el primero que la i al llegar a 0 lanzar� el resultado de la divisi�n numeroleido/i al Infinity, adem�s al no castear el resultadoDivision a double y cambiar su valor de variable de int a double no podr� mostrar los resultados correctos de las divisiones con decimales.
			resultadoDivision = (double) numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		//Al castear y cambiar la variable de resultadoDivision y cambiar el valor final de i a 1 ya se corrigen los fallos y realiza las divisiones de forma correcta
		lector.close();
	}

}
