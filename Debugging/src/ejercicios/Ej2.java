package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		
		
		System.out.println("Introduce un numero como String");
		input.nextLine(); //El error se produce al no poner un input.nextLine(); que preceda a la lectura de la cadena ya que al no vaciar el buffer no deja leer la cadena que se desea introducir
		cadenaLeida = input.nextLine();
		//Se puede solucionar simplemente a�adiendo el input.nextLine(); antes de la lectura de cadena
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		}
		
		input.close();
		
		
		
	}
}
