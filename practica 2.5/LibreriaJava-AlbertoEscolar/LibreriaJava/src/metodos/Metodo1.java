package metodos;

public class Metodo1 {

	public static int sumar(int a, int b)
    {
        return a + b;
    }
    public static String piramide(int n)
    {
        String temp = "";
        for (int linea = 1; linea <= n; linea++)
        {
            for (int i = 1; i <= linea; i++)
            {
                temp += " "+linea;
            }
            temp += "\n";
        }
        return temp;
    }
    public static String EsPar(int n)
    {
       
     if (n % 2 == 0)
        {
            return "par";
            
        }
        else
        {
            return "impar";
        }
     
    }

    public static int MediaDosNumeros (int a, int b)
    {
        return (a + b) / 2;
    }

    public static int restar (int a, int b)
    {
        return a - b;
    }

}
