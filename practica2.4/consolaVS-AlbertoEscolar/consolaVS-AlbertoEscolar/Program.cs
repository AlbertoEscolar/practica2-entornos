﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consolaVS_AlbertoEscolar
{
    class Program
    {

        static void Main(string[] args)
        {
            
            System.Console.WriteLine("***MENU VISUAL***");
            System.Console.WriteLine("1. Suma de valores");
            System.Console.WriteLine("2. Crear Piramide");
            System.Console.WriteLine("3. ¿Es primo?");
            System.Console.WriteLine("4. ¿Es par?");
            System.Console.WriteLine("Elije un metodo:");
            String respuesta = System.Console.ReadLine();
            int n=int.Parse(respuesta);
            switch (n)
            {
                case 1:
                    SumaValores();
                    break;
                case 2:
                    CrearPiramide();
                    break;
                case 3:
                    EsPrimo();
                    break;
                case 4:
                    EsPar();
                    break;
                default:
                    break;
            }

            Console.WriteLine("Presiona una tecla para continuar");
            Console.ReadKey();
        }


        private static void EsPar()
        {
            int n;
            System.Console.WriteLine("Introduce un numero: ");
            String respuesta=System.Console.ReadLine();
            n = int.Parse(respuesta);
            if (n % 2 == 0)
            {
                System.Console.WriteLine("El numero es par");
            }
            else
            {
                System.Console.WriteLine("El numero es impar");
            }
        }
        private static void EsPrimo()
        {
            
            int n, contadorRestos = 0;

            do
            {
                System.Console.WriteLine("Numero >0: ");
                String respuesta =System.Console.ReadLine();
                n = int.Parse(respuesta);
                if (n <= 0)
                {
                    System.Console.WriteLine("El numero tiene que ser positivo");
                }

            } while (n <= 0);

            int resto;
            for (int i = 1; i <= n; i++)
            {
                resto = n % i;
                if (resto == 0)
                    contadorRestos++;
            }
            if (contadorRestos == 2)
            {
                System.Console.WriteLine("El numero " + n + " es primo");
            }
            else
            {
                System.Console.WriteLine("No es primo");
            }

        }
        private static void CrearPiramide()
        {
            int linea;
            System.Console.WriteLine("Introduzca valor más grande: ");
             String respuesta=System.Console.ReadLine();
            int n = int.Parse(respuesta);
            for (linea = 1; linea <= n; linea++)
            {
                for (int i = 1; i <= linea; i++)
                {
                    System.Console.Write(linea);
                }
                System.Console.WriteLine();
            }

        }
        private static void SumaValores()
        {
            System.Console.WriteLine("Dime un numero");
            int n1;
            String respuesta = System.Console.ReadLine();
            n1=int.Parse(respuesta);
            System.Console.WriteLine("Dime otro numero");
            int n2;
            String respuesta2 = System.Console.ReadLine();
            n2 = int.Parse(respuesta2);

            int resultado =int.Parse (respuesta + respuesta2);
            System.Console.WriteLine("El resultado es " + resultado);



        }
    }
}
